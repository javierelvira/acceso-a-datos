package ficheros;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Examen114 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner leerNumeros = new Scanner(System.in);
		
		int numero1;
		int numero2;
		
		File carpeta,
			 fichero;
		
		System.out.println("primer numero: ");
		numero1 = leerNumeros.nextInt();
		System.out.println("segundo numero: ");
		numero2 = leerNumeros.nextInt();
		
		try 
		{
			carpeta = new File(Integer.toString(numero2));
			if (carpeta.mkdir() == true)
			{
				for ( int i = numero1; i <= numero2; i++) 
				{
					fichero = new File(Integer.toString(numero2) + "//" + Integer.toString(i) + ".num");
						fichero.createNewFile();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
