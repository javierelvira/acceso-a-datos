package ficheros;

import java.io.*;

public class Examen113 {

	public static void LeerFichero(String nombre, String[] tNombres, int[] tDni) {
		
		int resultado; //variable para leer
		int i; //indice de las tablas
		int numeroEmpleados; 
		String valorCadena; //para convertir los datos le�dos
		boolean numero,  //Indica si se lee un numero
				primero; //si es el primer numero
		
		
		try {
			File fichero = new File(nombre);
			FileReader fr = new FileReader(fichero);
			
			numero = true;
			primero = true;
			i=0;
			valorCadena = "";
			resultado = fr.read();
			
			while (resultado!=-1) {
				if(resultado != '-') {
					valorCadena += (char)resultado;
				}
				else {
					if(numero == true) 
					{
						if (primero == true) {
								numeroEmpleados = Integer.parseInt(valorCadena);
								primero = false;
						}
						else {
							tDni[i] = Integer.parseInt(valorCadena);
							i++;
						}
						numero = false;
					}
					else
					{// si numero es falso leemos un nombre
						tNombres[i] = valorCadena;
						numero = true;
					}
					valorCadena = "";
				}
				resultado = fr.read();
			}
			fr.close();
		} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void escribirPantalla(String[] tNombres, int[] tDni) {
		
		System.out.println("Contenido: ");
		for (int i = 0; i<tNombres.length; i++)
		{
			System.out.println(tNombres[i] + " - "+ tDni[i]);
		}
	}
	
	public static void mayor(String[] tNombres, int[] tDni) {
		
		int dniMayor = -100000, posMayor = 0;
		
		for(int i = 0; i<tDni.length; i++) 
		{
			if (tDni[i] > dniMayor)
			{
				dniMayor = tDni[i];
				posMayor = i;
			}
		}
		System.out.println("El nombre del dni  mayor es " + tNombres[posMayor]);
	}
	
	public static void menor(String[] tNombres, int[] tDni) {
		
		int dniMenor = -100000, posMenor = 0;
		
		for(int i = 0; i<tDni.length; i++) 
		{
			if (tDni[i] < dniMenor)
			{
				dniMenor = tDni[i];
				posMenor = i;
			}
		}
		System.out.println("El nombre del dni  mayor es " + tNombres[posMenor]);
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String[] tablaNombres = new String[4];
		int[] tablaDNIs = new int[4];
		
		LeerFichero("Empleados.txt", tablaNombres, tablaDNIs);
		escribirPantalla(tablaNombres, tablaDNIs);
		mayor(tablaNombres, tablaDNIs);
		menor(tablaNombres, tablaDNIs);
	}

}
