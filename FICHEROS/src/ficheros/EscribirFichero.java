package ficheros;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class EscribirFichero {

public static  void escribirFichero(String nombre, int[] tabla) {
		
		try {
			File fichero = new File(nombre);
			FileWriter fw = new FileWriter(fichero);
			
			for(int i = 0; i<tabla.length; i++)
			{
				fw.write(tabla[i] + "\n");
			}
			
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
public static void leerFichero(String nombre, int[] tabla) {
	
	int resultado,
	numero,
	i,
	numeroEntero;
	
	String numeroCadena;
	
	try {
		File fichero = new File(nombre);
		FileReader fr = new FileReader(fichero);

		i = 0;
		numeroCadena = "";
		resultado = fr.read();
		while ( resultado != -1)
		{
			if ((resultado == '\n') || (resultado == '\r'))
			{
				numeroEntero = Integer.parseInt(numeroCadena);
				tabla[i] = numeroEntero;
				numeroCadena = "";
				i++;
				if (resultado =='\r')
				resultado = fr.read();
			}
			else 
				numeroCadena += (char)resultado;
			resultado = fr.read();
		}
		fr.close();		
		for (int cnt = 0; cnt < 10; cnt++) 
			System.out.print(tabla[cnt]+ " ");
			System.out.println("");
		
		
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

public static void leerFicheroV2(String nombre, int[] tabla) {
	
	int resultado,
	numero,
	i,
	numeroEntero;
	
	String numeroCadena;
	
	try {
		File fichero = new File(nombre);
		FileReader fr = new FileReader(fichero);

		i = 0;
		numeroCadena = "";
		numeroEntero = 0;
		resultado = fr.read();
		while ( resultado != -1)
		{
			if (resultado == '\n')
			{
				tabla[i] = numeroEntero;
				i++;
				numeroEntero = 0;
			}
			else 
				numeroEntero = (numeroEntero*10) + (resultado -'0');
			resultado = fr.read();
		}
		fr.close();		
		for (int cnt = 0; cnt < 10; cnt++) 
			System.out.print(tabla[cnt]+ " ");
			System.out.println("");
		
		
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

}
