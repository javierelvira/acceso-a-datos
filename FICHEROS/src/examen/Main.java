package examen;

import java.io.*;


public class Main {

	public static void LeerCategoria(String n, Categoria[] cat1) {
		
		
		try {
			File fichero = new File(n);
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);
			
			String linea, nombre, idCat;
			int i=0;
			
			
			linea = br.readLine();
			while (linea != null) {
				idCat = linea;
				nombre = br.readLine();
				cat1[i] = new Categoria(idCat, nombre);
				i++;
				linea = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void LeerProducto(String n, Producto[] Pro1) {
		
		
		try {
			File fichero = new File(n);
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);
			
			String linea, nombre, idPro, idCat;
			int i=0;
			
			
			linea = br.readLine();
			while (linea != null) {
				idPro = linea;
				nombre = br.readLine();
				idCat = br.readLine();
				Pro1[i] = new Producto(idPro, nombre, idCat);
				i++;
				linea = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void escribir(Categoria cat[], Producto pro[]) {
		
		int np;
		String idCat, idProdCat;
		for (int i=0; i<cat.length; i++) 
		{
			np=0;
			idCat = cat[i].getId();
			for (int cnt=0; cnt<pro.length; cnt++)
			{
				System.out.println(cnt);
				idProdCat = pro[cnt].getIDC();
				if ( idCat.equals(idProdCat) )  
					np++;
			}
			System.out.println("Categoria " + cat[i].getNombre() + ": " + np);
		}
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Categoria[] tablaCategorias = new Categoria[2];
		Producto[] tablaProductos = new Producto[2];
		
		LeerCategoria("FicheroCategoria.txt", tablaCategorias);
		LeerProducto("FicheroProducto.txt", tablaProductos);
		escribir(tablaCategorias, tablaProductos);
	}

}
