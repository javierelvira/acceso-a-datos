package examen;

public class Producto {

	private String IDProducto;
	private String Nombre;
	private String IDCategoria;
	
	public Producto(String IDP, String prod, String IDC) {

		IDProducto = IDP;
		Nombre = prod;
		IDCategoria = IDC;
	}
	
	public String getIDP() {
		return IDProducto;
	}
	
	public String getNombre() {
		return Nombre;
	}
	
	public String getIDC() {
		return IDCategoria;
	}
}
