package buffer;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class Buffer {

private String nombreFichero = "texto.txt";
	
	public Buffer(String nombre) {
		nombreFichero = nombre;
	}

	public void EscribirTabla(String[] tabla) {
		try {
			File fichero = new File(nombreFichero);
			FileWriter fw = new FileWriter(fichero);
			BufferedWriter bw = new BufferedWriter(fw);
			for(int cnt=0; cnt<tabla.length; cnt++) {
				bw.write(tabla[cnt]);
				bw.newLine();
			}
		}catch(IOException e) {System.out.println(e.getMessage());}
		
		

	}

	
	public void LeerTabla(String[] tabla) {
		try {
			File fichero = new File(nombreFichero);
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);
			int cnt=0;
			String linea = "";
			
			linea = br.readLine();
			while(linea != null) {
				tabla[cnt] = linea;
				cnt++;
				linea = br.readLine();		//Se usa para que lo pise y acabe
			}
			br.close();

		}catch(IOException e) {System.out.println(e.getMessage());}
		
		

	}


}
