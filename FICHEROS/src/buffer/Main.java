package buffer;

public class Main {

	public static void MostrarPantalla(Persona[] tabla)
	{
		for(int cnt=0; cnt<tabla.length; cnt++)
		{
			System.out.println("Nombre: "+ tabla[cnt].getName());
			System.out.println("Edad: "+ tabla[cnt].getEdad());
		}
	}
	
	
	public static void vaciarTabla(Persona[] tabla)
	{
		for(int cnt=0; cnt<tabla.length; cnt++)
		{
			tabla[cnt].setNombre("");
			tabla[cnt].setEdad(0);
			
			tabla[cnt] = new Persona();
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String[] miTabla = {"Juan", "Ana", "Pedro", "Laura"};
		String[] otraTabla = new String[10];
		
		Buffer fich = new Buffer("fichero.txt");
		
		fich.EscribirTabla(miTabla);
		fich.LeerTabla(otraTabla);
		
		for(int cnt=0; cnt < otraTabla.length; cnt++) {
			System.out.println("Pos: " + cnt + " " + otraTabla[cnt]);
		
		}
		
		//TABLA PERSONAS
		Persona[] miPersona = new Persona[3];
		
		miPersona[0] = new Persona("Juan",19);
		miPersona[1] = new Persona("AnaIsabel", 12);
		miPersona[2] = new Persona("Lorenzo", 23);
		
		MostrarPantalla(miPersona);
		
		FicheroAlumnos FA = new FicheroAlumnos("F1.txt");
		FA.EscribirTabla(miPersona);
		
		vaciarTabla(miPersona);
		MostrarPantalla(miPersona);
		FA.LeerTabla(miPersona);
		MostrarPantalla(miPersona);
		
		
		//1. Escribir la tabla ordenada por edad y otra por nombre en el fichero (sin modificar la tabla original)
		
		
		
	}
}
