package buffer;

public class Persona {

	private String nombre;
	private int edad;
	
	public Persona() {
		nombre = "";
		edad = 1;
	}
	
	public Persona(String n, int e) {
		nombre = n;
		edad = e;
	}

	public String getName() {
		return nombre;
	}
	
	public int getEdad() {
		return edad;
	}
	
	public void setNombre(String n)
	{
		nombre = n;
	}
	
	public void setEdad(int e)
	{
		edad = e;
	}
	
}
