package buffer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FicheroAlumnos {

	private String nombreFichero;
	
	public FicheroAlumnos (String n) {
		
		nombreFichero = n;
	}
	
	public void EscribirTabla(Persona[] tabla) {
		try {
			File fichero = new File(nombreFichero);
			FileWriter fw = new FileWriter(fichero);
			BufferedWriter bw = new BufferedWriter(fw);
			for(int cnt=0; cnt<tabla.length; cnt++) {
				// Escribir una linea para cada dato: nombre y edad
				bw.write(tabla[cnt].getName());
				bw.newLine();
				bw.write(Integer.toString(tabla[cnt].getEdad()));
				bw.newLine();
			}
			bw.close();
		}catch(IOException e) {System.out.println(e.getMessage());}
		
	}
	public void LeerTabla(Persona[] tabla) {
			try {
				File fichero = new File(nombreFichero);
				FileReader fr = new FileReader(fichero);
				BufferedReader br = new BufferedReader(fr);
				int cnt=0;
				String linea = "";
				
				linea = br.readLine();
				while(linea != null) {
					tabla[cnt].setNombre(linea);
					linea = br.readLine();
					tabla[cnt].setEdad(Integer.parseInt(linea));
					cnt++;
					linea = br.readLine();		//Se usa para que lo pise y acabe
				}
				br.close();

			}catch(IOException e) {System.out.println(e.getMessage());}
		}
}

