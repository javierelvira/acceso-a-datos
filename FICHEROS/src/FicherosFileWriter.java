import java.io.*;

public class FicherosFileWriter {
	
	
	public static void LeerFichero(String nombre, String[] nombres) {
		
		
		try {
			//1. Apertura del fichero
			File fichero = new File(nombre);
			FileReader fr = new FileReader(fichero);
			
			//2. Lectura del fichero
			int letra;
			int posicion = 0;
			String cadena;
			
			cadena = "";
			letra= fr.read();
			while (letra != -1)
			{
				if (letra == '\n') {
					nombres[posicion] = cadena;
					cadena = "";
					posicion++;
				}
				else {
					//Tratamiento del caracter leido
					cadena += ((char) letra);
				}

				//Leer el siguiente car�cter
				//for(int i = 0; i<4; i++)
				letra= fr.read();
			}
			//3.Cerrar el fichero
			fr.close();
			
			//4. Escribir la cadena por pantalla
			System.out.println("He le�do: " + cadena);
			
		} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void EscribirFichero(String nombre, String[] nombres) {
		
		try {
			//1. Apertura del fichero
			File fichero = new File("FicheroTexto1.txt");
			FileWriter fw = new FileWriter(fichero);
			
			//2. Escribir en el fichero
			for(int i = 0; i<nombres.length; i++)
			{
				fw.write(nombres[i] + ";");
			}
			
			//3. Cerrar fichero
			fw.close();
			
			//4. llamar a leer el fichero
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
	}
	
public static void EscribirFicheroSaltos(String nombre, String[] nombres) {
		
		try {
			//1. Apertura del fichero
			File fichero = new File("FicheroTexto1.txt");
			FileWriter fw = new FileWriter(fichero);
			
			//2. Escribir en el fichero
			for(int i = 0; i<nombres.length; i++)
			{
				fw.write(nombres[i] + "\n");
			}
			
			//3. Cerrar fichero
			fw.close();
			
			//4. llamar a leer el fichero
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		
	}
	
	public static void InicializarTabla(String[] nombres) {
		for (int i=0; i<nombres.length; i++)
			nombres[i] = "";
	}
	
	
	public static void EscribirTabla(String[] nombres) {
		System.out.println("Contenido de la tabla: ");
		for(int i=0; i<nombres.length; i++) {
			System.out.print(nombres[i] + " ");
		}
	}
	
	public static void EscribirTablaSaltos(String[] nombres) {
		System.out.println("Contenido de la tabla: ");
		for(int i=0; i<nombres.length; i++) {
			System.out.print(nombres[i] + "\n");
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		String[] listaNombres = { "Pepe", "Javi", "Juan", "Alex"};
		
		//Llamada a escribir fichero
		//EscribirFichero("FicheroTexto1.txt", listaNombres);
		EscribirFicheroSaltos("FicheroTexto2.txt", listaNombres);
		
		InicializarTabla(listaNombres);
		
		//EscribirTabla(listaNombres);
		EscribirTablaSaltos(listaNombres);
				
		//Llamada a leer Fichero
		LeerFichero("FicheroTexto1.txt", listaNombres);
		//EscribirTabla(listaNombres);
		EscribirTablaSaltos(listaNombres);
	}

}
