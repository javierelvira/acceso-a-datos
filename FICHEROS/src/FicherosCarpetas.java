import java.io.File;

public class FicherosCarpetas {

	public static void main(String[] args) {
		
		String[] rutas = {"Nombre",
				"Nombre\\bin", 
				"Nombre\\bin\\bytecode", 
				"Nombre\\bin\\objetos", 
				"Nombre\\src", 
				"Nombre\\src\\clases", 
				"Nombre\\doc", 
				"Nombre\\doc\\html", 
				"Nombre\\doc\\pdf"};
		
		boolean seguir= true;
		
		for(int cnt=0; (cnt<rutas.length) && seguir; cnt++) {
			File carpeta = new File(rutas[cnt]);
			if( carpeta.mkdir()==false)
				seguir = false;
			else 
				seguir = true;
		}
	}
	
}
