// ConsoleApplicationFicheroPersona.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include <stdio.h>
#include <string.h>


//nombre y edad
//maximo 10 alumnos

//opcion 1: cargar datos coger datos del fichero y meterlos
//opcion 2: escribir datos
//opcion 3: insertar tabla
//opcion 4: escribir tabla en pantalla
//opcion 5: borrar tabla (atencion a los huecos que quedan)
//opcion 6: salir

/*---------------------------------------*/
/*Declaración de la estructura de alumnos*/
/*---------------------------------------*/

struct TAlumno
{
	char nombre[40];
	int edad;
};

/*-----------------------*/
/*       Funciones       */
/*-----------------------*/

void cargarDatos(char nombreFichero[50], struct TAlumno tablaAlumnos[10], int *numero)
{
	FILE * pf;
	char nombreAlumno[40];
	int edadAlumno,
	posicion = 0;

	pf = fopen(nombreFichero, "rt");
	if (pf != NULL)
	{
		while (!feof(pf))
		{
			fscanf(pf, "%s", nombreAlumno);
			fscanf(pf, "%i", &edadAlumno);
			strcpy(tablaAlumnos[posicion].nombre, nombreAlumno);
			tablaAlumnos[posicion].edad = edadAlumno;
			posicion++;
		}
		(*numero) = posicion;
		
		fclose(pf);
	}
}

void guardarDatos(char nombreFichero[50], struct TAlumno tablaAlumnos[10], int numero)
{
	FILE *pf;
	int posicion;

	pf = fopen(nombreFichero, "wt");
	if (pf != NULL)
	{
		for (posicion = 0; posicion < numero; posicion++)
		{
			fprintf(pf, "%s\n", tablaAlumnos[posicion].nombre);
			fprintf(pf, "%i\n", tablaAlumnos[posicion].edad);
		}
		fclose(pf);
	}
}

void imprimirTabla(struct TAlumno alumno[10])
{
	for (int cnt = 0; cnt < 11; cnt++)
	{
		printf("El nombre es %s y la edad es %i \n\n", alumno[cnt].nombre, alumno[cnt].edad);
	}
}

void escribirDatos(struct TAlumno alumno[10])
{
	char opcion[2];

	do
	{
		printf("Dime el nombre: ");
		scanf("%s", alumno[0].nombre);
		printf("Dime la edad: ");
		scanf("%i", &alumno[0].edad);
		printf("Quiere meter otro alumno: si/no\n");
		scanf("%s", opcion);
	} while (opcion == "si");
}

void menu() {
	printf("-----------------------------\n");
	printf("1. Cargar datos de un fichero\n");
	printf("2. Escribir datos\n");
	printf("3. Escribir tabla en pantalla\n");
	printf("4. Insertar datos en tabla\n");
	printf("5. Borrar dato de tabla\n");
	printf("6. Salir\n");
	printf("-----------------------------\n");
	printf("Introduce la opcion:");
}

/*------------------------*/
/*   Programa Principal   */
/*------------------------*/

int main(int argc, char const *argv[])
{
	int opcion;
	struct TAlumno tablaAlumnos[10];
	int numeroAlumnos;

	numeroAlumnos = 0;

	do
	{
		menu();
		scanf("%d", &opcion);
		switch (opcion)
		{
		case 1: cargarDatos("clase.txt", tablaAlumnos, &numeroAlumnos);
			break;

		case 2: guardarDatos("clase2.txt", tablaAlumnos, numeroAlumnos);
			break;

		case 3:imprimirTabla(tablaAlumnos);
			break;

		case 4: escribirDatos(tablaAlumnos);
			break;
/*
		case 5:

		case 6:*/
		}
	} while (opcion != 6);
	
    return 0;
}


