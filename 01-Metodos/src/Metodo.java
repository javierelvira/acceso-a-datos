
public class Metodo {

	public static int sumar(int n1, int n2)
	{
		int resultado;
		resultado=n1+n2;
		return resultado;
	}
	
	public static int sumarNumero(int n1, Numero n)
	{
		int resultado;
		resultado = n1 + n.DevuelveValor();
		return resultado;
	}
	
	
	public static void main(String[] args)
	{
		/*
		 * int num1 = 23, num2 = 41, res;
		 * 
		 * res = sumar(num1,num2); System.out.println(res);
		 */
		
		Numero  miNumero = new Numero(21),
				otroNumero = new Numero(33),
			   	esteNumero = new Numero(4);
		int 	numero1 = 3,
				resultado;

		System.out.println("El primero es " + miNumero.DevuelveValor());
		System.out.println("El segundo es " + otroNumero.DevuelveValor());
		
		
		//resultado = miNumero +1;
		resultado = miNumero.Sumar(1);
		System.out.println(resultado);
		
		//Llamada a metodo static
		resultado = Numero.Sumar(21, 33);
		System.out.println(resultado);
		
		//Lamada con parametros de clase
		resultado = sumarNumero(numero1, esteNumero);
		System.out.println(numero1);
		System.out.println(esteNumero.DevuelveValor());
		System.out.println(resultado);
	}

}
