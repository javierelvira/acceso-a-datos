
public class Numero 
{
	// Atributos
	private int valor;

	
	//Constructor
	public Numero(int v) 
	{
		valor = v;
	}
	
	public int Sumar(int v)
	{
		int resultado;
		resultado = valor + v;
		return resultado;
	}

	public int DevuelveValor()
	{
		return valor;
	}
	
	public void FijarValor(int nuevo)
	{
		valor = nuevo;
	}
	
	
	public static int Sumar(int n1, int n2)
	{
		int resultado;
		resultado=n1+n2;
		return resultado;
	}
	
}
